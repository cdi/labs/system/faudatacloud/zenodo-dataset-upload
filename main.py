import requests
# How to create an access token: https://zenodo.org/account/settings/applications/tokens/new/
from zenodo_access_token import ACCESS_TOKEN
import json


def file_upload(path, filename, meta_data, publish):
    ##############################################
    # settings like url and access token
    ##############################################
    params = {'access_token': ACCESS_TOKEN}
    headers = {"Content-Type": "application/json"}
    # ToDo: change to live URL
    base_url = 'https://sandbox.zenodo.org/api/deposit/depositions'

    ##############################################
    # create empty entry
    ##############################################
    r = requests.post(base_url,
                      params=params,
                      json={},
                      headers=headers)
    assert r.status_code == 201
    print('#########################\nemtpty create\n#########################\n', r.status_code,
          json.dumps(r.json(), indent=2))

    links = r.json()['links']
    print(links)

    # Wait for ES to index.
    # sleep(1)

    ##############################################
    # file upload
    ##############################################

    full_path = path + '/' + filename
    # The target URL is a combination of the bucket link with the desired filename
    # seperated by a slash.
    bucket_url = r.json()["links"]["bucket"]
    with open(full_path, "rb") as fp:
        r = requests.put(
            "%s/%s" % (bucket_url, filename),
            data=fp,
            params=params,
        )

    assert r.status_code == 200
    print('#########################\nfile upload\n#########################\n', r.status_code,
          json.dumps(r.json(), indent=2))

    ##############################################
    # add meta data
    ##############################################
    r = requests.put(
        links['self'],
        data=json.dumps(dict(metadata=meta_data)),
        params=params,
        headers=headers
    )
    assert r.status_code == 200
    print('#########################\nmeta-data\n#########################\n', r.status_code,
          json.dumps(r.json(), indent=2))

    ##############################################
    # Publish
    ##############################################

    if publish:
        r = requests.post(links['publish'], params=params, headers=headers)
        assert r.status_code == 202
        print('#########################\nPublish\n#########################\n', r.status_code,
              json.dumps(r.json(), indent=2))


if __name__ == '__main__':
    # hello_zenodo()
    # empty_upload()
    meta_data = {
        'title': 'My 2nd upload',
        'upload_type': 'publication',
        'publication_type': 'book',
        'description': 'This is my first upload',
        'access_right': 'open',
        'license': 'cc-by',
        'creators': [{'name': 'Doe, Michi', 'affiliation': 'Zenodo'}]
    }
    publish = False

    file_upload('.', 'example.zip', meta_data, publish)
