# zenodo-dataset-upload

Im Rahmen des FauDataCloud Prozesses soll es möglich sein Datasets bei Zenodo hochzuladen (<50 GB).

## Benutzung:

1. persönliches [access token](https://zenodo.org/account/settings/applications/tokens/new/) erstellen
2. Je nachdem ob Test oder echter Upload: URL anpassen
3. "file_upload(...)" ausführen

### base_url:
```python
    base_url = 'https://sandbox.zenodo.org/api/deposit/depositions'
    base_url = 'https://zenodo.org/api/deposit/depositions'
```

### example:
```python
def my_custom_upload_function():
    publish = True
    meta_data = {
        'title': 'My upload',
        'upload_type': 'publication',
        'publication_type': 'book',
        'description': 'This is my first upload',
        'access_right': 'open',
        'license': 'cc-by',
        'creators': [{'name': 'Doe, John', 'affiliation': 'Zenodo'}]
    }

    file_upload('.', 'example.zip', meta_data, publish)
````

ToDos:  
- [x] dataset/file upload
- [ ] reading?
- [ ] deleting?
